# using a different way to make the fake data after Sofia's idea that the noise is making a mess of things for us with the simple sinusoid. 

import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
#import cPickle as pickle
#import plotFFT

# the primary measured signal containing the GW, clutter line and noise
def getPrimarySignal(refSignal,gwSignal,clutterAmp=2.,
                     noiseAmp=2.,psiRefOne=0.235,
                     refSignalTwo=False,psiRefTwo=0.343):  

    ## parameters ## 
    # psi: this is to shift the reference signal before adding to the primary
    #      so that is it NOT the same phase as the measured reference signal 
    #      we give to the ANC code
    # noiseAmp: the amplitude of the random noise added to the primary signal 

    # shifting the clutter line in phase before making the primary signal
    clutterLine = clutterAmp * (refSignal * np.exp(1.j*psiRefOne)).real

    # second reference channel 
    if len(refSignalTwo)!=1:
        clutterLine2 = clutterAmp * (refSignalTwo * np.exp(1.j*psiRefTwo)).real
        primarySignal = clutterLine + clutterLine2 + gwSignal + noiseAmp*np.random.randn(len(refSignal))        
        return primarySignal

    # the primary signal = clutter + gw + noise
    primarySignal = clutterLine + gwSignal + noiseAmp*np.random.randn(len(refSignal))

    return primarySignal # clutter line for debugging only, not used in ANC



# this is the small gravitational wave signal at freqGW 
def getGWSignal(time,freqGW=4.0, h=0.05, phi=0.34):
    ## parameters ##
    # freqGW: the gravitatioanl wave line's frequency
    # h: gw amplitude
    # phi: phase of gw signal#
    
    g = h * np.sin((2. * np.pi * freqGW * time) + phi)

    return g


# the reference signal 
def getReferenceSignal(time,freqC=5.2,N=5,
                       wn=np.array([0.41,0.04,0.22,0.57,0.35]),
                       an = np.array([-0.03,0.10,-0.16,0.08,-0.07]),
                       phin=np.array([0.0,0.1,0.2,0.3,0.4]),
                       refAmp=0.1,refNoiseAmp=0.1,twoChannels=False):

    ## parameters
    # N: the number of weighted sinusoids
    # wn: weight of sinusoids
    # an: wandering frequency of clutter line 
    # refNoiseAmp: amplitude of noise added to reference signal 
    
    # need to loop over t here so that we can do the sum over wn and an
    complexRef = np.zeros(len(time))
    for i,t in enumerate(time):

        complexRef[i] = sum( wn * np.exp(2.j * np.pi * (freqC + an)*t + phin) ) 

    realRef = refAmp*complexRef.real + refNoiseAmp*np.random.randn(len(complexRef.real)) # added some noise


    # Second Ref Channel - phase shifted and different noise
    if twoChannels==True:
        phaseShift = np.random.rand()
        realRef2 = refAmp*(complexRef* np.exp(1.j*phaseShift)).real + refNoiseAmp*np.random.randn(len(complexRef.real)) 
        return realRef,realRef2



    #plt.plot(time,realRef)        
    #plt.show()
    return realRef  # complex ref not needed




def writeLines(times,primary,ref,inj,name='tmp.dat',ref2=False):
    dataFile = open(name,'w')

    if len(ref2)!=1:
        dataFile.write('time\tprimary\treference1\treference2\tinjected\n')
        for t,p,r1,r2,i in zip(times,primary,ref,ref2,inj):
            dataFile.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format(t,p,r1,r2,i)) 

    else: 
        dataFile.write('time\tprimary\treference\tinjected\n')
        for t,p,r,i in zip(times,primary,ref,inj):
            dataFile.write('{0}\t{1}\t{2}\t{3}\n'.format(t,p,r,i)) 

    dataFile.close()
    return




def saveParams(dataParams, gWParams, refSigParams, 
               primarySigParams, name='tmp.dat',directory='./'):
    paramFile = open('{0}{1}'.format(directory,name),'w')
   
    for key, value in dataParams.iteritems():
       paramFile.write("{0}\t{1}\n".format(key, value))
    for key, value in gWParams.iteritems():
       paramFile.write("{0}\t{1}\n".format(key, value))
    for key, value in refSigParams.iteritems():
       paramFile.write("{0}\t{1}\n".format(key, value))
    for key, value in primarySigParams.iteritems():
       paramFile.write("{0}\t{1}\n".format(key, value))

    paramFile.close()

    return 
#testing here



import configparser
import argparse


parser = argparse.ArgumentParser(description=__doc__,
                  formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('--inifile', '-i', required=True, type=str, 
                     help='Path to ini file')
args = parser.parse_args()

iniFile = str(args.inifile)


config = configparser.ConfigParser()
config.read(iniFile)


###########################
##### data parameters #####
###########################
startTime = float(config['data']['start_time'])
endTime   = float(config['data']['end_time'])
srate     = float(config['data']['srate'])
timeStep = 1./srate
times = np.arange(startTime,endTime,timeStep)
seed = int(config['data']['seed'])
np.random.seed(seed)



####################################
###### ref/clutter parameters ######
####################################
fC           = float(config['clutter']['centralFreq'])
weightSines  = np.atleast_1d([float(str_val) for str_val in config['clutter']['weightSines'].split(',')])
freqsSines   = np.atleast_1d([float(str_val) for str_val in config['clutter']['freqSines'].split(',')])
phaseSines   = np.atleast_1d([float(str_val) for str_val in config['clutter']['phaseSines'].split(',')])


noSinusoids  = int(len(weightSines)) # N 
noiseAmpRef  = float(config['clutter']['noiseAmp']) #0.01 
referenceAmp = float(config['clutter']['refAmp'])   #0.1 #0.1#1.E-19
#realRefSig, realRefSigTwo = getReferenceSignal(times, freqC=fC, N=noSinusoids, wn=weightSines, an=freqsSines, refAmp=referenceAmp, refNoiseAmp=noiseAmpRef, twoChannels=True)


################################
##### Injection parameters #####
################################
fGW   = float(config['injection']['freq'] )#59.98
hAmp  = float(config['injection']['hAmp']) #5e-19 # THIS IS THE h AMPLITUDE IN THE PRIMARY
phiGW = float(config['injection']['phi']) #0.0
#gravWaveSignal = getGWSignal(times,freqGW=fGW,h=hAmp,phi=phiGW)


###############################
##### Primary parameters ######
###############################
primaryNoiseAmp     = float(config['primary']['primNoiseAmp']) #1.E-18 #0.00000001#2.0#1.E-19 #2.0#0.01# 2.0
clutterAmpInPrimary = float(config['primary']['clutterAmpInPrim']) #THIS IS THE REF AMPLITUDE IN THE PRIMARY






# save parameters used to generate fake data    
dirName = '.'.format(fC,fGW)


# trying to avoid loading everything into memory at once and save each time step as we go
#dataSaveFileName='{0}/fC{1}_fGW{2}_hAmp{3}.dat'.format(dirName,fC,fGW,hAmp)
dataSaveFileName='data.dat'.format(fC,fGW,hAmp)
rollingSaveFile = open(dirName+'/'+dataSaveFileName, 'w')
rollingSaveFile.write('time\tprimary\treference1\tinjected\n')
batchSize=10240
batchNo=int((srate*endTime)/batchSize)
print ('no. batches: ', batchNo)
tStart=0.0

for b in range(batchNo):
    tEnd = tStart+(batchSize/srate)
    times = np.arange(tStart,tEnd,timeStep)
    realRefRoll, realRefTwoRoll = getReferenceSignal(times, freqC=fC, N=noSinusoids, \
                                                     wn=weightSines, an=freqsSines, \
                                                     phin=phaseSines,\
                                                     refAmp=referenceAmp, \
                                                     refNoiseAmp=noiseAmpRef, \
                                                     twoChannels=True)
    gWRoll = getGWSignal(times,freqGW=fGW,h=hAmp,phi=phiGW)
    primRoll = getPrimarySignal(realRefRoll, gWRoll, clutterAmp=clutterAmpInPrimary, \
                                            noiseAmp=primaryNoiseAmp,refSignalTwo=realRefTwoRoll, \
                                            psiRefOne=0.111) 

    for t,p,r,i in zip(times,primRoll,realRefRoll,gWRoll): 
        rollingSaveFile.write('{0}\t{1}\t{2}\t{3}\n'.format(t,p,r,i)) 

    tStart=tEnd
rollingSaveFile.close()




"""

# seed, dataParams, refSigParams, gWSigParams, primarySigParams
dataParams = { "randomSeed": seed, 
               "startTime": startTime, 
               "endTime": endTime, 
               "timeStep": timeStep, 
               "samplingRate": int(srate) }
gWParams = { "gwFrequency": fGW, 
             "gwAmplitude": hAmp, 
             "gwPhase": phiGW}
refSigParams = { "clutterFrequency": fC, 
                 "numberSinusoids": noSinusoids, 
                 "sinusoidWeights": weightSines, 
                 "sinusoidFrequencies": freqsSines, 
                 "referenceSigNoiseAmp": noiseAmpRef}
primarySigParams = { "primarySigNoiseAmp": primaryNoiseAmp,
                     "clutterAmpInPrimary": clutterAmpInPrimary}



dataDuration = int(endTime - startTime)
runDetails = 'srate{0}_dur{1}_cAmpInPrim{2}_hAmp{3}'.format(int(dataParams["samplingRate"]),dataDuration,clutterAmpInPrimary,hAmp)

paramFileName = ('params_{1}.dat'.format(dirName,runDetails))
dataFileName  = ('data_{1}.dat'.format(dirName,runDetails))


#print 'writing fake data to file'
#writeLines(times,primarySignal,realRefSig,gravWaveSignal,name=dataFileName,ref2=realRefSigTwo)
#print 'done'
print ('saving data parameters')
saveParams(dataParams, gWParams, refSigParams, primarySigParams,name=paramFileName,directory=dirName)
print ('done')

"""



#plt.plot(times,primarySignal,alpha=0.7,color='r')
#plt.plot(times,clutter, alpha=0.7, color='c')
#plt.plot(times,realRefSig,alpha=0.7,color='g')
#plt.plot(times,gravWaveSignal,alpha=0.7,color='k')
#plt.show()


exit()





def makeFFT(time,value): 
    n = len(time)
    print(n/2)
    k = np.arange(n)
    T = max(time)
    frq = k/T
    frq = frq[range(int(n/2))]

    fft = np.fft.fft(value) / n
    fft = fft[range(int(n/2))]

    return frq, fft


#colours
orange='#E69F00'
blue='#0072B2'
green='#009E73'
ltblue='#56B4E6'
vermillion='#D55E00'
yellow='#F0E442'
pink='#CC79A7'

dataIdentifier='fC60.0_fGW59.98_hAmp1e-19'
dataToPlot = 'ARLS/data/testSet/{}.dat'.format(dataIdentifier)
dataToPlot = 'test.dat'

data = np.genfromtxt(dataToPlot,names=True)

time = data['time']
prim = data['primary']
ref = data['reference1']
inj = data['injected']


frqP,fftPrim = makeFFT(time,prim)
frqR,fftRef = makeFFT(time,ref)
frqI,fftInj = makeFFT(time,inj)

plt.clf()
plt.clf()
fig, ax = plt.subplots(3, 1)

#plt.vlines(60.01,1E-24,1E-18,color='g',alpha=0.3)
ax[0].plot(frqP,abs(fftPrim),alpha=0.6,color='k',label='primary',lw=2)
#ax[0].plot(frqF,abs(fftFilt),alpha=0.7,color=orange,label='filtered',lw=2)


#ax[0].plot(frqF,abs(fftFilt),alpha=0.7,color=orange,label='filtered',lw=2)


ax[0].legend()
#ax[0].set_xlim(59.5,60.5)
#plt.xlim(1,130)

#ax[0].set_ylim(1E-22,3E-18)
ax[0].set_xlabel('frequency / Hz')
ax[0].set_ylabel('|asd| (fft)')
ax[0].set_yscale('log')
ax[0].set_xlim(58, 62)


ax[2].plot(frqI,abs(fftInj),alpha=0.6,color='b',label='injection',lw=2)
ax[2].legend()
ax[2].set_xlim(58, 62)


print(fftRef)
print (frqR)
ax[1].plot(frqR,abs(fftRef),alpha=0.6,color=vermillion,label='reference',lw=2)
#ax[1].set_xlim(59.5,60.5)
ax[1].set_ylim(min(abs(fftRef)),max(abs(fftRef)))
ax[1].legend()
#ax[1].set_ylim(1E-5,3E-2)
ax[1].set_ylabel('|asd| (fft)')
ax[1].set_xlabel('frequency / Hz')
ax[1].set_yscale('log')
ax[1].set_xlim(58,62)



plt.tight_layout()
plt.savefig('test2.pdf')
plt.show()














