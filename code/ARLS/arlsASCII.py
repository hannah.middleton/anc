
"""
#!/usr/bin/env python

Sofia's ARLS code (Sat Oct 13 08:32:12 2018) with some modifications to 
prevent reading in the whole file at once. 

This will take a file of format XXXXXXX
and create filtered version of the data. 

It will write the results as it goes. 
"""

import numpy as np
import scipy.fftpack as ff
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt



def runningARLS(fileName,order,filteredResultsDir='./',scaleReference=1.,lambd=0.9999):

  resultsFile=open(filteredResultsDir+'/filteredData.dat','w')
  resultsFile.write('time\tprim\tref\tfilt\n')

  with open('{0}'.format(fileName)) as fileobject:

    # take first line as these are the headers 
    firstLine = fileobject.readline().split()

    #n = len(primary)
    delayed = np.zeros(order)
    adap = np.zeros(order)
    #cancelled = np.zeros(n)
    I=np.eye(order)

    P=I
    for ki,line in enumerate(fileobject):
       
        columns = line.split()
        time, primary, reference = float(columns[0]),\
                                   float(columns[1]),\
                                   float(columns[2])


        delayed[0] = reference/scaleReference
        cancelled = primary-delayed.dot(adap)
        #delayed[0] = reference[ki]
        #cancelled[ki] = primary[ki]-delayed.dot(adap)


        K =np.array( P.dot(delayed)/(lambd+(delayed.dot(P)).dot(delayed)))[0]
        
        P = (I-np.transpose(np.mat(K))*np.mat(delayed)).dot(P)/lambd

        adap = adap + cancelled*K
        #print K
        delayed[1:order] = delayed[0:order-1]


        wLength=10240 
        if ki/wLength == ki//wLength and ki!=0: 
            np.savetxt('{0}/filter/adap_{1}.txt'.format(filteredResultsDir,ki), adap)
            #plt.clf()
            #plt.bar(np.arange(0,len(adap),1), adap)
            #plt.savefig('{0}/weights/weights_checking-{1}.png'.format(filteredResultsDir,ki))
        else: pass

        """

        columns = line.split()
        time, primary, reference = float(columns[0]),\
                                   float(columns[1]),\
                                   float(columns[2])


        delayed[0] = reference/scaleReference
        cancelled = primary-delayed.dot(adap)
        #delayed[0] = reference[ki]
        #cancelled[ki] = primary[ki]-delayed.dot(adap)


        K =np.array( P.dot(delayed)/(lambd+(delayed.dot(P)).dot(delayed)))[0]
        adap = adap + cancelled*K
        P = (I-np.transpose(np.mat(K))*np.mat(delayed)).dot(P)/lambd

        
        #print K
        delayed[1:order] = delayed[0:order-1]


        wLength=10240 
        if ki/wLength == ki//wLength and ki!=0: 
            np.savetxt('{0}/weights/adap_{1}.txt'.format(filteredResultsDir,ki), adap)
            #plt.clf()
            #plt.bar(np.arange(0,len(adap),1), adap)
            #plt.savefig('{0}/weights/weights_checking-{1}.png'.format(filteredResultsDir,ki))
        else: pass



        print (ki)
        columns = line.split()
        time, primary, reference = float(columns[0]),\
                                   float(columns[1]),\
                                   float(columns[2])

        delayed[0] = reference/scaleReference 
        pi = P.dot(delayed )
        #print (pi)
        K =np.array( P.dot(delayed)/(lambd+(delayed.dot(P)).dot(delayed)))[0]
        
        #print (adap)
        #print(delayed)
        cancelled = primary - delayed.dot(adap)
        print (cancelled)
        print(K)
        adap = adap + cancelled * K
        P = (1./lambd) * (P - np.transpose(np.mat(K)).dot(delayed).dot(P))
        #P =  (1./lambd) * (I - np.transpose(np.mat(K))*np.mat(delayed)).dot(P)

        delayed[1:order]=delayed[0:order-1]
        """
        resultsFile.write('{0}\t{1}\t{2}\t{3}\n'.format(time,primary,
                                                        reference,cancelled))
        
    resultsFile.close()

    return [cancelled, adap]


