"""
#!/usr/bin/env python

a version of the ARLS filter which will take GW frame files as the 
data input to be filtered 
It reads in the data in batches as the frame files are opened and
read all at once (no line by line reading)
"""

import numpy as np
#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt
#from gwpy.timeseries import TimeSeries, TimeSeriesDict
#import gwpy.signal.fft
#from astropy import units as u


# takes in primary.value, reference.value....
def batchARLS(order,timeseries,adap,delayed,P,primary,reference,lambd): 

    # setup for filter
    #lambd = 0.9999
    n = len(primary)
    #delayed = np.zeros(order)
    #adap = np.zeros(order)
    #cancelled = np.zeros(n)
    I=np.eye(order)
    #P=I

    #print (adap, 'in func') 
    # run the filter on this batch
    for i in range(n):

        delayed[0] = reference[i]
        cancelled = primary[i] - delayed.dot(adap)
       
        K = np.array( P.dot(delayed)/(lambd+(delayed.dot(P)).dot(delayed)) )[0]
        P = (I - np.transpose(np.mat(K)) * np.mat(delayed)).dot(P) / lambd
        adap = adap + cancelled*K

        delayed[1:order] = delayed[0:order-1]
    
    
        timeseries.value[i] = cancelled
    #print (adap, 'in func')
    return adap, delayed, P, timeseries

