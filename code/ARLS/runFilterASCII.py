"""
#!/usr/bin/env python

To run the filter on the data - this is specific for NOT ligo 
data and only reads in ascii files
Need to adapt to read in frame files for real LIGO data
"""

import argparse
import os
import arlsASCII

def getCommandLineArguments():


    parser = argparse.ArgumentParser(description=__doc__,
                      formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--datafile', '-d', dest='dataFileToFilter', 
                         required=True, type=str, 
                         help='Path to file containing data to be filtered')
    parser.add_argument('--order', '-o', dest='order', required=True, type=int,
                         help='Order for the filter.')
    parser.add_argument('--lambda', dest='lambd',required=False,
                         type=float, default=0.9999, 
                         help='set the forgetting factor for the filter')
    parser.add_argument('--resultsDir', '-r', dest='filtedResultsDir', 
                         required=False, 
                         help='Path to dir where filtered data will be saved')
    parser.add_argument('--refScale', dest='refScalingFactor',required=False,
                         type=float, default=1.0, 
                         help='add a scaling factor if reference is too large')
    args = parser.parse_args()

    return args


def main():

    # setup for the filter
    args = getCommandLineArguments()

    # command line arguments
    unfilteredData = args.dataFileToFilter
    order = args.order
    resultsLocation = args.filtedResultsDir

    # if the results directory does not exist, this will make it
    if not os.path.exists(resultsLocation):
        os.makedirs(resultsLocation)
    if not os.path.exists(resultsLocation+'/filter'):
        os.makedirs(resultsLocation+'/filter')   

    print("running the ARLS...")
    arlsASCII.runningARLS(unfilteredData, order, filteredResultsDir=resultsLocation, scaleReference=args.refScalingFactor,lambd=args.lambd)

    print("done")



if __name__ == '__main__':
    main()

