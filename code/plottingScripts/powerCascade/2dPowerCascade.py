import matplotlib
matplotlib.use('Agg')
from gwpy.timeseries import TimeSeries
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.collections import PolyCollection
import numpy as np


fig = plt.figure()
ax = fig.gca(projection='3d')

def gaussian():
    xs = np.arange(59.0,61.0,0.01)
    xo = 59.98+np.random.randn()*0.02
    print (xo)
    y = np.exp(-(xs-xo)**2. / (2.*0.1))
    return xs,y



def offset(myFig,myAx,n=1,yOff=60):
    #shift = yOff / myFig.dpi
    #dx, dy = 0., shift
    #print(myFig.dpi,yOff,shift)
    shift = 0.02
    dx, dy = 0., shift
    return (myAx.transData + mpl.transforms.ScaledTranslation(dx,n*dy,myFig.dpi_scale_trans)), shift



def getData(startTime,batchSize,fLimLow,fLimHigh): 
    print("""
    startTime {}
    """.format(startTime))
    endTime = startTime + batchSize
    batchNo = batchSize/64

    resampleRate = 1064
    referenceChannelName = str('H1:PEM-CS_MAINSMON_EBAY_1_DQ')

    times = np.arange(startTime, endTime, 64)
    refFiles= ['/archive/frames/O2/raw/H1/H-H1_R-{}/H-H1_R-{}-64.gwf'.format(str(startTime)[:5], t) for t in times]
    #print (refFiles)
    reference = TimeSeries.read(refFiles,referenceChannelName,start=startTime,\
                                end=endTime,resample=resampleRate)

    refASD = reference.asd()
    #print (refASD.value)
    #print (refASD.df)

    fstart = refASD.f0.value
    fend = len(refASD.value)*refASD.df.value
    fstep = refASD.df.value
    #print('''
    #fstart {}
    #fstep  {}
    #fend   {}
    #'''.format(fstart,fstep,fend))

    fs = np.arange(fstart,fend,fstep)

    indexLow  = int( fLimLow / fstep )+1
    indexHigh = int( fLimHigh/ fstep )

    val = refASD.value

    #fs[fs<indexLow]= np.nan
    #fs[fs>indexHigh]= np.nan
    #val[val<indexLow] = np.nan
    #val[val>indexHigh] = np.nan
    #return fs, val
    return fs[indexLow:indexHigh], val[indexLow:indexHigh]
   


def cc(arg):
    return mcolors.to_rgba(arg, alpha=0.6)




fig, ax = plt.subplots(figsize=(5,6))
sampling = 2.
noToPlot =210

thetas= np.arange(0,sampling*noToPlot,sampling)
number = len(thetas)
# for color plot
#cmap = mpl.cm.get_cmap('viridis')
#norm = mpl.colors.Normalize(vmin=0,vmax=360)


startTime = 1166401536
batchSize = 64*5
fPlotLow  = 59.5
fPlotHigh = 60.5


for it, t in enumerate(thetas):

  zInd = number - it

  trans, shift = offset(fig,ax,it,yOff=sampling)

  time = startTime+int(it*batchSize)

  xs, ys = getData(time, batchSize,fPlotLow,fPlotHigh)
  log10ys = np.log10(ys)
  #xs, ys = gaussian()

  ax.plot(xs,log10ys,color='k',linewidth=0.5,transform=trans, zorder = zInd)
  #ax.fill_between(xs,ys,-0.5,facecolor='w',edgecolor="None", transform=trans, zorder = zInd)

ax.set_xlim(fPlotLow, fPlotHigh)
ax.set_xlabel('Frequency (Hz)')
ax.grid(False)
#ax.set_xlim(59.5,60.5)
ax.set_ylim(-3,max(log10ys)+(noToPlot*shift)+95)
#ax.set_ylim(min(np.log10(ys)),max(np.log10(ys))*noToPlot)
#ax.set_ylim(0.0,1.5)
for side in ["top", "right", "left"]:
    ax.spines[side].set_visible(False)
#ax.set_xticks([])
plt.tick_params(axis='x', top='off')
# labelbottom='off', right='off', left='off', labelleft='off')
ax.set_yticks([])
plt.savefig('/home/hannah.middleton/public_html/mainsPower/cascade/cascadeTest.pdf')




























# not used
###########################################################################

exit()


#xs = np.arange(0, 10, 0.1)
verts = []
zs = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]

startTime = 1166401536
batchSize = 128

zMax = -500

fPlotLow  = 59.8
fPlotHigh = 60.2

for z in zs:

    time = startTime + int(z*batchSize)
    #ys = np.random.rand(len(xs))
    xs, ys = getData(time,batchSize,fPlotLow,fPlotHigh)
    #ys[0], ys[-1] = 0, 0
    print(min(xs))
    if max(ys)>zMax: zMax = max(ys)

   

    verts.append(list(zip(xs-60, ys)))
#print(verts)
poly = PolyCollection(verts, facecolors='w', edgecolors='k')
#  facecolors=[cc('r'), cc('g'), cc('b'),
#                                         cc('y')])
poly.set_alpha(0.7)
ax.add_collection3d(poly, zs=zs, zdir='y')


ax.set_xlabel('X')
ax.set_xlim3d(fPlotLow-60, fPlotHigh-60)
ax.set_ylabel('Y')
ax.set_ylim3d(-1, 10)
ax.set_zlabel('Z')
ax.set_zscale('log')
ax.set_zlim3d(0, zMax)
ax.view_init(elev=40., azim=270)

#ax.set_zaxis_off()
ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))

plt.savefig('/home/hannah.middleton/public_html/mainsPower/cascade/cascadeTest.png')
plt.savefig('/home/hannah.middleton/public_html/mainsPower/cascade/cascadeTest.pdf')
plt.show()
