'''
plots results of ARLS with fake data
'''

import numpy as np
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.fftpack

import argparse

parser = argparse.ArgumentParser(description=__doc__, 
                                 formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('--results', '-r', dest='pathToResultsFile', type=str, 
                    required=True, help='The path to the results file to be plotted')
args = parser.parse_args()


# read in the data
data = np.genfromtxt(args.pathToResultsFile, names=True)

# make FFTs (real)
pf = scipy.fftpack.rfft(data['prim'])
ff = scipy.fftpack.rfft(data['filt'])
rf = scipy.fftpack.rfft(data['ref'])


T = data['time'][1]-data['time'][0]
sampleRate = int(1./T)

N=len(data['time'])

xf = scipy.fftpack.rfftfreq(N, 1/sampleRate)

# plotting FFT
#plt.axvline(59.999, alpha=0.2, ls='--')
plt.plot(xf,np.abs(pf),alpha=0.5,label='primary')
plt.plot(xf,np.abs(ff),alpha=0.5,label='filtered',ls='--')
plt.xlim(59.9,60.1)
plt.ylabel('FFT')
plt.xlabel('Frequency (Hz)')
plt.legend()
plt.show()
#
plt.clf()
plt.plot(xf, np.abs(pf)-np.abs(ff))
plt.xlim(59.9,60.1)
plt.show()
