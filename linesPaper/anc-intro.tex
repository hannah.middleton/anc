

Gravitational wave (GW) observatories such as Advanced LIGO~\cite{AasiEtAlAdLIGO:2015} and Advanced Virgo~\cite{AcerneseEtAlAdVirgo:2015} require high sensitivity to make GW detections. 
To meet these sensitivity needs, GW observatories employ a variety of techniques to isolate them from environmental noise. 
However they cannot be fully isolated and detector data contains short (glitches~\cite{DavisEtAl:2020}) and long (narrow spectral~\cite{CovasEtAl:2018}) duration noise artifacts. 


Mitigation and removal of noise spectral lines is an area of active study. 
GW detectors have a wealth of physical environmental monitors (PEM) which record environmental effects on the instrument and are used to assess the quality of the data at any one time~\cite{DetCharGW150914:2016,MarinShoemakerWeissPEM:1997}. 
Much effort has been made to identify the source of lines and, wherever possible, to remove the source of the noise. 
For example, in Ref.~\cite{CovasEtAl:2018}, a comb artifact (with $1\,{\rm Hz}$ spacing and $0.5\,{\rm Hz}$ offset) is identified in Observing Run 1 data to originate from blinking LEDs in the timing system for the observatory. 
The effect was reduced by preventing the LEDs from flashing. 

Preventing the process causing noise lines is not always possible and in many cases the cause remains unidentified. 
It can, however, be possible to remove the effect of a noise line after data acquisition. 
Information from PEMs provides a useful resource for removing noise from the GW data if there is a correlation between the PEM channel and the noise in the GW channel. 
Recent work has used PEM channels to remove noise spectral lines with machine learning techniques~\cite{VajenteEtAl:2020, OrmistonEtAl:2020}. 


Noise lines pose a particular difficulty to searches for continuous waves (CWs); persistent, periodic GW signals which are expected to be emitted by rotating neutron stars. 
Searches may cover the entire sky~\cite{AllSkyO2:2019,EinsteinAtHomeAllSky:2021} or target specific objects such as millisecond pulsars~\cite{O3PulsarSearch:2020,KnownPulsarsTwoHarminics:2019,KnownPulsarSearchO1:017}, low mass x-ray binaries~\cite{ScoX1ViterbiO2, SearchCrossCorrO1:2017, MeadorsEtAlS6LMXBSearch:2017, ScoX1ViterbiO1:2017, RadiometerO1O2:2019, SearchRadiometerO1:2017, MiddO2LMXBs:2020}, and supernova remnants~\cite{SNRO3a:2021, MillhouseEtAlSNR:2020, LindblomOwenSNR:2020, SNRSearch:2019}.%, and post-merger remants?[].  
The noise lines are often loud and can obscure a CW signal in the affected frequency bins. 
CW candidates in proximity to noise line are typically vetoed (e.g. Ref.~\cite{ScoX1ViterbiO2}).
Efforts have been made to reduce the vulnerability of CW searches to noise lines~\cite{BayleyEtAl:2020,KeitelEtAl:2014}.



In this study we investigate the prospects of adaptive noise cancellation~\cite{WidrowEtal:1975} (ANC) for line removal in GW data. 
We focus on the removal of the $60\,{\rm Hz}$ noise line due to interference from the United States power grid. 
ANC can be applied to situations where a reference signal provides an independent measurement of the noise interference, similar to the methods applied in Refs.~\cite{VajenteEtAl:2020,OrmistonEtAl:2020}. 
The PEM data recorded at each observatory provides an excellent opportunity to test the effectiveness of ANC for GW data. 


In Sec.~\ref{sec:method} we describe the method of ANC used in this work and in Sec.~\ref{sec:60Hz} we discuss the behaviour of the power line in the GW data and the available environmental monitors. 
In Sec.~\ref{sec:results} we test the algorithm using GW data and discuss our conclusions in Sec.~\ref{sec:conclusion}. 




