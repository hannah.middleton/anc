
ANC provides an estimate of a signal which has been corrupted by some interference or noise. 
The method can be applied in the situation where a dataset is corrupted by interference as long as there is an independent witness to the interference. 
In this work we refer to the two timeseries as the \emph{primary} and \emph{reference}. 
The primary contains the information of interest, plus some additional unwanted interference. 
The reference contains a measurement of the interference which is correlated in some unknown way to the noise in the primary signal. 
For our purposes, the primary is the gravitational-wave channel and the reference is a PEM recording data from the power grid (see Sec.~\ref{sec:60Hz}). 
The aim of the ANC is to use the reference to compute an estimate of the noise as it appears in the primary and subtract it. 
In this section, we describe the implementation of ANC used in this work. 

The primary timeseries $d_i$ (evenly sampled at time steps $i = 0,\dots,N$) is known to contain an unwanted interference, which we label the `clutter' $c_i$. 
The primary also contains other noise $n_i$, and may contain a signal of interest $h_i$ (the GW), so that
\begin{equation}
d_i = h_i + c_i + n_i\,.
\label{eqn:primary}
\end{equation}
The objective is to remove $c_i$ with the aid of a reference measurement $r_i$ whilst leaving $h_i$ intact. 
It is likely that the relationship between $r_i$ and $c_i$ is unknown; the coupling of the mains interference into the detector may be extremely complex. 
We use $r_i$ to construct an estimate $y_i$ of $c_i$ and remove it in the time domain. 
The filtered timeseries is
\begin{eqnarray}
e_i &~=~& d_i - y_i \,, \\
    &~\approx~& h_i + n_i \,.
\end{eqnarray}
The value of $y_i$ is the scalar product of the tap-input vector $\mathbf{u}$ and the tap-weight vector $\mathbf{w}$. 
The tap-input $\mathbf{u}$ is taken directly from the reference signal and is defined as
\begin{equation}
u_k = [r_k, r_{k-1}, \dots, r_{k-M+1}]\,,
\end{equation}
where $M$ is the order of the adaptive filter. 
There are different methods to compute $\mathbf{w}$. 
We use the adaptive recursive least squares method described below. 


\subsection{Adaptive Recursive Least Squares Method}
\label{sec:arlsrecipee}

In this work we use the adaptive recursive least squares (ARLS) method to compute the estimated signal. 
The method is described in Chapter 9 of Ref.~\cite{HaykinAdaptiveFT:2002}. 

ARLS requires two input parameters, the order $M$ and $\lambda$, the meaning of which is discussed below. 
We described the algorithm as pseudocode and it is also illustrated by the block diagram in Fig.~\ref{fig:arlsBlock}. 
\begin{enumerate}
\item Initialise zero value $M$-by-$1$ vectors $\mathbf{u(n=0)}$ and $\mathbf{w(n=0)}$ for the tap-input and tap-weights respectively for timestep $n=0$. .
\item Initialise empty $M$-by-$M$ matrix $\mathbf{P}$, which is referred to as the inverse correlation matrix. 
\item Read in the values of $d_n$ and $r_n$ at the current time $n$. 
\begin{enumerate}
\item The tap inputs are updated as 
\begin{equation}
u_{k=0} = r(n) \,,
\end{equation}
\item and the estimated signal subtracted
\begin{eqnarray}
e_n &=& d_n - y_n\,, \\
    &=& d_n - \mathbf{w}\mathbf{u}. 
\end{eqnarray}
\item Compute the gain vector $\mathbf{K}$ (an $M$-by-$1$ vector)
\begin{equation}
\mathbf{K} = \frac{ \mathbf{P} \mathbf{u} }{ \lambda + \mathbf{u} \mathbf{P}\mathbf{u}}
\end{equation}
\item and update $\mathbf{P}$
\begin{equation}
\mathbf{P} = \frac{1}{\lambda} ( \mathbf{I} - \mathbf{K}^T \mathbf{u} ) \mathbf{P}\,.
\end{equation}
\item The tap-weights are updated 
\begin{equation}
\mathbf{w(n+1)} = \mathbf{w} + y_n \mathbf{K}
\end{equation}
\item and the elements of the tap inputs are shifted by one so that the next reference value takes the first position (see step 3a). 
\end{enumerate}
\item Repeat step 3. until $n=N$
\end{enumerate}

\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{images/blockDiagram.pdf}
\end{center}
\caption{\label{fig:arlsBlock}
Block diagram of the ARLS method described in Sec~\ref{sec:arlsrecipee}. 
}
\end{figure}
%A note on the pareamster
Intuitively, $\lambda$ can be thought of as the forgetting factor of the filter~\cite{HaykinAdaptiveFT:2002}. 
It is a positive constant, close to but less than $1$. 
A value of $\lambda=1$ corresponds to infinite memory and the filter becomes an ordinary least squares method.
In this work we use $\lambda=0.9999$. 
The value of $M$ depends on the delay between the interference as it is recorded in the PEM and the GW channel. 
In Sec.~\ref{sec:results} we trial a selection of $M$ values for GW data. 




